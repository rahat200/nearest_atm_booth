package com.example.googlemapatm;

import java.util.List;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;

import com.example.googlemapatm.adapter.SearchAdapter;
import com.example.googlemapatm.db.AtmDAO;
import com.example.googlemapatm.db.SQLiteHelper;
import com.example.googlemapatm.domain.Atm;
import com.example.googlemapatm.map.AtmMap;
import com.example.googlemapatm.map.NearestAtmMap;
import com.example.googlemapatm.map.SearchResultMap;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

public class MapViewActivity extends FragmentActivity {

	int bankID;
	SQLiteHelper db;
	
	private Menu menu;
	
	GoogleMap googleMap;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		db = SQLiteHelper.getInstance(this);
		
		bankID = getIntent().getIntExtra("BANK-ID", 1);
		
		setABTitle(R.string.title_atm);
		new AtmMap(this, getGoogleMap(), bankID).buildMap();
		
	}
	
	private void handleIntent(Intent intent) {
		
		if(Intent.ACTION_VIEW.equals(intent.getAction())) {
			String atmID = intent.getDataString();
			Atm atm = new AtmDAO(this).getSingleAtm(atmID);
			new SearchResultMap(this, getGoogleMap(), atm).buildMap();
		}
		
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		handleIntent(intent);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
	}
	
	
	public GoogleMap getGoogleMap() {
		
		setUpMapIfNeeded();
		return googleMap;
	}
	
	
	private void setUpMapIfNeeded() {
		if(googleMap != null) {
			return;
		}
		googleMap = ((MapFragment) getFragmentManager()
				.findFragmentById(R.id.map)).getMap();
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.main, menu);
		this.menu = menu;
		
	    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	    	 
	        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
	 
	        SearchView search = (SearchView) menu.findItem(R.id.ab_Search).getActionView();
	 
	        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
	 
	        search.setOnQueryTextListener(new OnQueryTextListener() { 
	 
	            @Override 
	            public boolean onQueryTextChange(String query) {
	            	loadSearchResult(query);
	 
	                return true; 
	            }

				@Override
				public boolean onQueryTextSubmit(String query) {
					return false;
				} 
	 
	        });
	        
	    }

		return true;
	}

	private void loadSearchResult(String query) {
		
		if(query.length() > 0 ) {
			
			String sql = "SELECT id AS _id, id AS "+SearchManager.SUGGEST_COLUMN_INTENT_DATA+" , name FROM "
					+SQLiteHelper.ATM_TABLE
					+" WHERE name LIKE '%"+ query.toLowerCase() +"%'";
			
			Cursor cursor = db.query(sql, null);
			
			SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
			 
			
	        final SearchView search = (SearchView) menu.findItem(R.id.ab_Search).getActionView(); 
	 
	        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
	 
	        search.setSuggestionsAdapter(new SearchAdapter(this, cursor));
	        
	        
		}
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		case R.id.ab_Search:
			return true;
			
		case R.id.ab_atm_menu:
			setABTitle(R.string.title_atm);
			//set atm markers in map
			new AtmMap(this, getGoogleMap(), bankID).buildMap();
			return true;
		
		case R.id.ab_nearest_menu:
			
			setABTitle(R.string.title_nearest);
			//set nearest atm marker in map
			new NearestAtmMap(this, getGoogleMap()).buildMap();
			return true;
		
		case R.id.ab_branch_menu:
			setABTitle(R.string.title_branch);
			//set branch markers in map
			new AtmMap(this, getGoogleMap(), bankID).buildMap();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
		
	}
	
	public void setABTitle(int titleid) {
		getActionBar().setTitle(titleid);
	}

}
