package com.example.googlemapatm;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.googlemapatm.adapter.BankListAdapter;
import com.example.googlemapatm.db.BankDAO;
import com.example.googlemapatm.db.SQLiteHelper;
import com.example.googlemapatm.domain.Bank;
import com.example.googlemapatm.utils.Utils;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.os.Build;

public class BankSelectActivity extends Activity {

	List<Bank> bankList;
	ListView listView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bank_select);

		listView = (ListView) findViewById(R.id.lv_banks);
		
		new BanksApiTask().execute();
		
		
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				
				Intent intent = new Intent(BankSelectActivity.this, MapViewActivity.class);
				intent.putExtra("BANK-ID", bankList.get(position).id);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bank_select, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	private class BanksApiTask extends AsyncTask<String, String, String> {

		ProgressDialog pd;
		int bankCount = 0;

		@Override
		protected void onPreExecute() {
			pd = new ProgressDialog(BankSelectActivity.this);
			pd.show();
		}

		@Override
		protected String doInBackground(String... params) {

			if (!new BankDAO(BankSelectActivity.this).getAllBanks().isEmpty()) {
				return null;
			}

			String apiRoot = getResources().getString(R.string.api_root);
			String url = apiRoot + getResources().getString(R.string.api_banks);

			SQLiteHelper db = SQLiteHelper.getInstance(BankSelectActivity.this);

			try {

				JSONObject json = new JSONObject(Utils.httpRequest(url, "GET",
						null, true));

				JSONArray allBanks = json.getJSONArray("banks");
				bankCount = allBanks.length();

				for (int i = 0; i < bankCount; i++) {
					JSONObject bank = allBanks.getJSONObject(i);

					ContentValues vals = new ContentValues();

					vals.put("short", bank.getString("short"));
					vals.put("name", bank.getString("name"));
					vals.put("id", bank.getInt("id"));

					db.insert(SQLiteHelper.BANK_TABLE, vals);

				}

			} catch (JSONException e) {
				Log.e("ERROR", "JSON Exception");
			} catch (Exception e) {
				Log.e("ERROR", "General Exception" + e);
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {

			bankList = new BankDAO(BankSelectActivity.this).getAllBanks();
			BankListAdapter adapter = new BankListAdapter(BankSelectActivity.this,bankList);
			
			listView.setAdapter(adapter);

			if (pd.isShowing())
				pd.cancel();
		}

	}


	
}
