package com.example.googlemapatm.adapter;

import com.example.googlemapatm.R;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class SearchAdapter extends CursorAdapter {

	TextView text;
	
	public SearchAdapter(Context context,Cursor cursor) {
		super(context, cursor, false);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		text.setText(cursor.getString(cursor.getColumnIndex("name")));
		
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 
        View view = inflater.inflate(R.layout.search_item, parent, false);
 
        text = (TextView) view.findViewById(R.id.tv_searchResult);
 
        return view;
	}
}
