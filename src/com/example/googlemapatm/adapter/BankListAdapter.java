package com.example.googlemapatm.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.googlemapatm.R;
import com.example.googlemapatm.domain.Bank;

public class BankListAdapter extends ArrayAdapter<Bank> {

	List<Bank> content_list = new ArrayList<Bank>();
	Context context;

	public BankListAdapter(Context context,
			List<Bank> list) {
		super(context,R.layout.list_row_bank , list);
		
		this.content_list = list;
		this.context = context;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		Bank bank = content_list.get(position);
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.list_row_bank, null);
		}
		((TextView) view.findViewById(R.id.tv_bankName)).setText(bank.name);
		return view;

	}

}
