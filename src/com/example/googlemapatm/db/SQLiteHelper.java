package com.example.googlemapatm.db;

import java.io.File;

import com.example.googlemapatm.utils.AppConstants;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class SQLiteHelper {

	private static final String DATABASE_NAME = "atmapp.db";
	private static final int DATABASE_VERSION = 1;
	private static final String TAG = "ATMAPP DB";
	
	public static final String BANK_TABLE = "banks";
	public static final String ATM_TABLE = "atms";
	
	private static String DB_PATH;
	
	private static SQLiteHelper dbHelper = null;

	private static SQLiteDatabase db = null;

	OpenHelper openHelper;
	private Context context;
	static String query;

	private SQLiteHelper(Context context) {
		this.context = context;

		DB_PATH = new File(AppConstants.getDatabasePath(context), AppConstants.DB_NAME).getAbsolutePath();
		
		if (db != null)
			if (db.isOpen())
				db.close();

		openHelper = new OpenHelper(this.context);
		db = openHelper.getWritableDatabase();

		dbHelper = this;

	}

	public void beginTrans() {
		db.beginTransaction();
	}

	public void endTrans() {
		db.endTransaction();
	}

	public static SQLiteHelper getInstance(Context context) {
		if (dbHelper == null) {
			dbHelper = new SQLiteHelper(context);
		}
		return dbHelper;
	}

	public synchronized void close() {
		if (openHelper != null) {
			openHelper.close();
		}
	}

	

	public synchronized long insert(String table, ContentValues values) {
		return db.insert(table, null, values);
	}

	public synchronized long insertOrIgnore(String table, ContentValues values) {
		return db.insertWithOnConflict(table, null, values,
				SQLiteDatabase.CONFLICT_IGNORE);
	}

	public synchronized int delete(String table, String whereClause,
			String[] whereArgs) {
		return db.delete(table, whereClause, whereArgs);
	}

	public synchronized int update(String table, ContentValues values,
			String whereClause, String[] whereArgs) {
		return db.update(table, values, whereClause, whereArgs);
	}

	public Cursor query(String sql, String[] whereValues) {
		return db.rawQuery(sql, whereValues);
	}

	public boolean exist(String sql, String[] whereValues) {
		Cursor foo = db.rawQuery(sql, whereValues);

		if (foo != null && foo.getCount() > 0) {
			foo.close();
			return true;
		}
		foo.close();
		return false;
	}

	static class OpenHelper extends SQLiteOpenHelper {
		OpenHelper(Context context) {
			super(context, DB_PATH, null, 
					DATABASE_VERSION);
		}

		public void onCreate(SQLiteDatabase db) {

			String bankSql = "CREATE TABLE "
					+ BANK_TABLE
					+ "(id INTEGER ,short TEXT, name TEXT );";
			String atmSql = "CREATE TABLE "
					+ ATM_TABLE
					+ "(id INTEGER, bank_id INTEGER, name TEXT, address TEXT, lat DOUBLE, "
					+ "lng DOUBLE, type TEXT);";
			

			db.execSQL(bankSql);
			db.execSQL(atmSql);
			

			Log.d(TAG, "db created");

		}

		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// String sql =
			// "alter table "+CATEGORY_TABLE+" change title title TEXT CHARACTER SET utf8;";
			// db.execSQL(sql);

		}

	}

}
