package com.example.googlemapatm.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;

import com.example.googlemapatm.domain.Atm;

public class AtmDAO {
	Context context;
	SQLiteHelper db;
	
	public AtmDAO(Context context) {
		this.context = context;
		db = SQLiteHelper.getInstance(context);
	}
	
	public List<Atm> getAllAtms(int bankId) {
		
		List<Atm> list = new ArrayList<Atm>();
		
		String sql = "SELECT * FROM " + SQLiteHelper.ATM_TABLE
				+ " WHERE bank_id = ?";
		Cursor cursor = db.query(sql, new String[]{String.valueOf(bankId)});
		
		if(cursor != null && cursor.moveToFirst()) {
			
			do {
				
				Atm atm = new Atm();
				atm.id = cursor.getInt(cursor.getColumnIndex("id"));
				atm.name = cursor.getString(cursor.getColumnIndex("name"));
				atm.type = cursor.getString(cursor.getColumnIndex("type"));
				atm.address = cursor.getString(cursor.getColumnIndex("address"));
				atm.lat = cursor.getDouble(cursor.getColumnIndex("lat"));
				atm.lng = cursor.getDouble(cursor.getColumnIndex("lng"));
				
				list.add(atm);
				
			} while (cursor.moveToNext());
		}
		
		cursor.close();
		
		return list;
	}
	
	public Atm getSingleAtm(String id) {
		
		String sql = "SELECT * FROM " + SQLiteHelper.ATM_TABLE
				+ " WHERE id = ?";
		Cursor cursor = db.query(sql, new String[]{id});
		
		Atm atm = new Atm();
		
		if(cursor.getCount() == 1 && cursor.moveToFirst()) {
				
			atm.id = cursor.getInt(cursor.getColumnIndex("id"));
			atm.name = cursor.getString(cursor.getColumnIndex("name"));
			atm.type = cursor.getString(cursor.getColumnIndex("type"));
			atm.address = cursor.getString(cursor.getColumnIndex("address"));
			atm.lat = cursor.getDouble(cursor.getColumnIndex("lat"));;
			atm.lng = cursor.getDouble(cursor.getColumnIndex("lng"));;
				
		}
		
		cursor.close();
		return atm;
	}
	
	
	public Atm findNearestAtm(double center_lat, double center_lng) {
		String sql = "SELECT *,(( "+center_lat+"-lat)*("+center_lat+"-lat) + ("+center_lng+"-lng)*("+center_lng+"-lng)) " +
				"AS distance from atms order by distance";
	
		Cursor cursor = db.query(sql, null);
		
		Atm atm = new Atm();
		
		if(cursor.moveToFirst() ) {
			 
			atm.id = cursor.getInt(cursor.getColumnIndex("id"));
			atm.name = cursor.getString(cursor.getColumnIndex("name"));
			atm.type = cursor.getString(cursor.getColumnIndex("type"));
			atm.address = cursor.getString(cursor.getColumnIndex("address"));
			atm.lat = cursor.getDouble(cursor.getColumnIndex("lat"));;
			atm.lng = cursor.getDouble(cursor.getColumnIndex("lng"));;
				
		}
		
		cursor.close();
		return atm;
		
	}
}
