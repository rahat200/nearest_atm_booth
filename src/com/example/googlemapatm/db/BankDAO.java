package com.example.googlemapatm.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;

import com.example.googlemapatm.domain.Bank;


public class BankDAO {
	Context context;
	SQLiteHelper db;
	
	public BankDAO(Context context) {
		this.context = context;
		db = SQLiteHelper.getInstance(context);
	}
	
	public List<Bank> getAllBanks() {
		
		List<Bank> list = new ArrayList<Bank>();
		
		String sql = "SELECT * FROM " + SQLiteHelper.BANK_TABLE;
		
		Cursor cursor = db.query(sql, null);
		
		if(cursor != null && cursor.moveToFirst()) {
			
			do {
				
				Bank bank = new Bank();
				bank.id = cursor.getInt(cursor.getColumnIndex("id"));
				bank.name = cursor.getString(cursor.getColumnIndex("name"));
				bank.short_name = cursor.getString(cursor.getColumnIndex("short"));
				
				list.add(bank);
				
			} while (cursor.moveToNext());
		}
		
		cursor.close();
		
		return list;
	}
}
