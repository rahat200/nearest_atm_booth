package com.example.googlemapatm.domain;

public class Atm {
	
	public int id;
	public String name;
	public String address;
	public String type;
	public double lat;
	public double lng;
}
