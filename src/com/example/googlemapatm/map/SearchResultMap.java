package com.example.googlemapatm.map;

import android.content.Context;

import com.example.googlemapatm.domain.Atm;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class SearchResultMap extends BaseMap {

	GoogleMap map;
	Context context;
	Atm atm;
	
	public SearchResultMap(Context context, GoogleMap map, Atm atm) {
		super(context, map);
		
		this.map = map;
		this.context = context;
		this.atm = atm;
		
	}

	@Override
	public void buildMap() {
		setCurrentPosition();
		addAtmInMap(atm);
		
	}
	
	private void addAtmInMap( Atm atm) {
		
		CameraPosition cameraPosition = new CameraPosition.Builder()
		.target(new LatLng(atm.lat, atm.lng))
		.zoom(13).build();
		
		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		
		map.getUiSettings().setAllGesturesEnabled(true);
		
		MarkerOptions marker = new MarkerOptions().position(
				new LatLng(atm.lat, atm.lng)).title(atm.name);
		
		marker.icon(BitmapDescriptorFactory
				.defaultMarker(BitmapDescriptorFactory.HUE_RED));
		
		map.addMarker(marker).showInfoWindow();
		
	}



}
