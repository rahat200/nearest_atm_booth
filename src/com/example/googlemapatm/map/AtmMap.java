package com.example.googlemapatm.map;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.googlemapatm.R;
import com.example.googlemapatm.db.AtmDAO;
import com.example.googlemapatm.db.SQLiteHelper;
import com.example.googlemapatm.domain.Atm;
import com.example.googlemapatm.utils.Utils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class AtmMap extends BaseMap {

	GoogleMap map;
	Context context;
	int bankID;
	public AtmMap(Context context, GoogleMap map, int bankID) {
		super(context, map);
		
		this.map = map;
		this.context = context;
		this.bankID = bankID;
		
	}

	@Override
	public void buildMap() {
		
		setCurrentPosition();
		new AtmApiTask().execute(String.valueOf(bankID),
				String.valueOf(0)); //time_after = 0 temporairily
		
	}
	
	private void setMarkersInMap(List<Atm> atmList) {

		for (Atm atm : atmList) {
			MarkerOptions marker = new MarkerOptions()
			.position(new LatLng(atm.lat, atm.lng)).title(atm.name)
			.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
;
			map.addMarker(marker);
		}
	}
	
	
	private class AtmApiTask extends AsyncTask<String, String, String> {
		int bankID;
		int atmCount = 0;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			pd = new ProgressDialog(context);
			pd.show();
		}

		@Override
		protected String doInBackground(String... params) {

			bankID = Integer.valueOf(params[0]);
			String time_after = params[1];

			if (!Utils.isNetworkAvailable(context)) {
				return null;
			}

			
			String apiRoot = context.getResources().getString(R.string.api_root);
			String apiAtm = apiRoot
					+ context.getResources().getString(R.string.api_atms);

			String api_url = apiAtm + "/bank/" + bankID + "/timeafter/"
					+ time_after;// `/api/atms/+ bank/1/timeafter/1233`

			SQLiteHelper db = SQLiteHelper.getInstance(context);

			try {
				JSONObject json = new JSONObject(Utils.httpRequest(api_url,
						"GET", null, true));
				JSONArray allAtms = json.getJSONArray("atms");
				atmCount = allAtms.length();
				if(atmCount == 0)
					return null;
				
				db.delete(SQLiteHelper.ATM_TABLE, null, null);
				
				for (int i = 0; i < atmCount; i++) {
					JSONObject atm = allAtms.getJSONObject(i);

					ContentValues vals = new ContentValues();

					vals.put("name", atm.getString("name"));
					vals.put("address", atm.getString("address"));
					vals.put("lat", atm.getString("lat"));
					vals.put("lng", atm.getString("lng"));
					vals.put("type", atm.getString("type"));
					vals.put("id", atm.getInt("id"));
					vals.put("bank_id", bankID);

					db.insert(SQLiteHelper.ATM_TABLE, vals);

				}

			} catch (JSONException e) {
				Log.e("ERROR", "JSON Exception");
			} catch (Exception e) {
				Log.e("ERROR", "General Exception" + e);
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {

			List<Atm> atmList = new AtmDAO(context).getAllAtms(bankID);
			setMarkersInMap(atmList);

			if (pd.isShowing())
				pd.cancel();
		}

	}

}
