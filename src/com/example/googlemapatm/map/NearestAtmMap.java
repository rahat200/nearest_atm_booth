package com.example.googlemapatm.map;

import java.text.DecimalFormat;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.googlemapatm.MapViewActivity;
import com.example.googlemapatm.R;
import com.example.googlemapatm.db.AtmDAO;
import com.example.googlemapatm.db.SQLiteHelper;
import com.example.googlemapatm.domain.Atm;
import com.example.googlemapatm.utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class NearestAtmMap extends BaseMap {

	GoogleMap map;
	Context context;
	Atm atm;
	
	LatLng current_latlng;
	
	public NearestAtmMap(Context context, GoogleMap map) {
		super(context, map);
		
		this.map = map;
		this.context = context;
		this.atm = atm;
		
	}

	@Override
	public void buildMap() {
		
		setCurrentPosition();
		
		current_latlng = getCurrentPosition();
		
		if(current_latlng != null) {

			addAtmInMap(new AtmDAO(context).findNearestAtm(current_latlng.latitude
					, current_latlng.longitude));
		}
		else {
			Log.e("ERROR", "current lat lng is not available");
		}
			
		
		
		
	}
	
	private void addAtmInMap( Atm atm) {
		
		
		CameraPosition cameraPosition = new CameraPosition.Builder()
		.target(new LatLng(atm.lat, atm.lng))
		.zoom(13).build();
		
		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		
		map.getUiSettings().setAllGesturesEnabled(true);
		
		MarkerOptions marker = new MarkerOptions().position(
				new LatLng(atm.lat, atm.lng)).title(atm.name);
		
		marker.icon(BitmapDescriptorFactory
				.defaultMarker(BitmapDescriptorFactory.HUE_RED));
		
		marker.snippet("Distance: "+ getDistanceBetweenTwoMarkers(current_latlng, new LatLng(atm.lat, atm.lng)) + " Km");
		
		map.addMarker(marker).showInfoWindow();
		
	}
	
	
	private String getDistanceBetweenTwoMarkers(LatLng src, LatLng dest) {
		
		//Haversine method to calculate distance between 2 lat lngs
		
		double R = 6371000; // radius of earth in meter
		
		double dLat = Math.toRadians(src.latitude - dest.latitude);
		double dLng = Math.toRadians(src.longitude - dest.longitude);
		
		double lat1 = Math.toRadians(src.latitude);
		double lat2 = Math.toRadians(dest.latitude);
		
		double a = Math.pow(Math.sin(dLat/2), 2) + 
				Math.pow(Math.sin(dLng/2), 2) * Math.cos(lat1) * Math.cos(lat2);
		
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		
		double distance = R * c;
		
		return new DecimalFormat("#0.00").format(distance/1000);
		
	}



}
