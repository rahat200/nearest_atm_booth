package com.example.googlemapatm.map;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.example.googlemapatm.utils.GPSTracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public abstract class BaseMap {

	GoogleMap map;
	Context context;
	
	public BaseMap(Context context, GoogleMap map) {
		this.map = map;
		this.context = context;
		
		this.map.clear();
	}
	
	public abstract void buildMap();
	
	protected void setCurrentPosition() {
		
		
		LatLng current_latlng = getCurrentPosition();
		if(current_latlng != null) {
			
			
			map.getUiSettings().setAllGesturesEnabled(true);
			map.getUiSettings().setCompassEnabled(true);
			
			setCameraPosition(current_latlng.latitude, current_latlng.longitude, calculateZoomLevel(getScreenWidth()));
			
			MarkerOptions marker = new MarkerOptions().position(
					new LatLng(current_latlng.latitude, current_latlng.longitude))
					.title("You are here");
			
			marker.icon(BitmapDescriptorFactory
					.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

			map.addMarker(marker).showInfoWindow();
		
		}
		else {
			Log.e("ERROR", "current lat lng is not available");
		}
			
		
	}
	
	protected LatLng getCurrentPosition() {
		LatLng latlng = null;
		
		GPSTracker gpsTracker = new GPSTracker(context);
		
		if(gpsTracker.canGetLocation() == true) {
			latlng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
		} else {
			gpsTracker.showSettingsAlert();
		}
		
		return latlng;
	}
	
	protected void setCameraPosition(double lat, double lng, float zoom) {
		
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), zoom));
		
	}
	
	private int calculateZoomLevel(int screenWidth) {
	    
		double equatorLength = 40075004; // in meters
	    double widthInPixels = screenWidth;
	    double metersPerPixel = equatorLength / 256;
	    int zoomLevel = 1;
	    while ((metersPerPixel * widthInPixels) > 5000) {
	        metersPerPixel /= 2;
	        ++zoomLevel;
	    }
	    return zoomLevel;
	}
	
	private int getScreenWidth(){
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		
		Point size = new Point();
		
		display.getSize(size);
		return size.x;
	}
}
