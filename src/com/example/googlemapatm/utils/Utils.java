package com.example.googlemapatm.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.format.DateFormat;
import android.util.Log;


public class Utils {

	public static String httpRequest(String url, String method,
			List<NameValuePair> params, boolean needResponse)
			throws IOException {
		InputStream inputStream = null;
		String responseText = "";

		try {

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpRequestBase httpReq = null;

			if (url.startsWith("https"))
				httpClient
						.getConnectionManager()
						.getSchemeRegistry()
						.register(
								new Scheme("SSLSocketFactory", SSLSocketFactory
										.getSocketFactory(), 443));

			if (method == "POST") {
				httpReq = new HttpPost(url);
				//Log.e("REQUEST", "req success url :" + url);// delete
				if (params != null)
					((HttpPost) httpReq).setEntity(new UrlEncodedFormEntity(
							params));
			} else if (method == "GET") {
				if (params != null) {
					String paramString = URLEncodedUtils
							.format(params, "utf-8");
					url += "?" + paramString;
					//Log.d("URL ACTION: ", "GET: " + url);
				}
				httpReq = new HttpGet(url);
			}

			HttpResponse httpResponse = httpClient.execute(httpReq);
			HttpEntity httpEntity = httpResponse.getEntity();
			boolean isOkay = httpResponse.getStatusLine().getStatusCode() == 200;
			Log.e("Status Code", "status code"
					+ httpResponse.getStatusLine().getStatusCode());

			if (needResponse && isOkay) {

				inputStream = httpEntity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inputStream, "iso-8859-1"), 8);
				
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				inputStream.close();
				reader.close();
//				responseText = sb.toString();
				
				responseText = new String(sb.toString().getBytes("ISO-8859-1"), "utf-8");
				//Log.e("response",""+responseText);
				
			}

		} catch (UnsupportedEncodingException e) {
			Log.d("HttpConnection", e.getMessage());
		} catch (ClientProtocolException e) {
			Log.d("HttpConnection", e.getMessage());
		} finally {

		}

		return responseText;

	}
	
	public static boolean isNetworkAvailable(Context context) {
	    ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo networkInfo = cm.getActiveNetworkInfo(); 
	    if (networkInfo != null && networkInfo.isConnected())
	        return true;
	    
	    return false;
	} 
	
	
	
}
