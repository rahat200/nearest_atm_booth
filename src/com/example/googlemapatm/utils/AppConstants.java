package com.example.googlemapatm.utils;

import java.io.File;

import android.content.Context;
import android.os.Environment;

public class AppConstants {

	public static String DB_NAME = "googlemapatm.db";
	
	public static File getDatabasePath(Context context) {
		return context.getExternalFilesDir(null);
	}

	
}
